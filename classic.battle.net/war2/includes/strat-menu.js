var	clean=1;
var	objName;

if (document.images)
{	news_off = new Image(121,35);
	news_off.src = "/images/battle/war2/images/strat-menu/news-off.gif";
	news_ovr = new Image(121,35);
	news_ovr.src = "/images/battle/war2/images/strat-menu/news-ovr.gif";
	news_act = new Image(121,35);
	news_act.src = "/images/battle/war2/images/strat-menu/news-act.gif";

	basic_off = new Image(121,35);
	basic_off.src = "/images/battle/war2/images/strat-menu/basic-off.gif";
	basic_ovr = new Image(121,35);
	basic_ovr.src = "/images/battle/war2/images/strat-menu/basic-ovr.gif";
	basic_act = new Image(121,35);
	basic_act.src = "/images/battle/war2/images/strat-menu/basic-act.gif";

	ovh_off = new Image(121,35);
	ovh_off.src = "/images/battle/war2/images/strat-menu/ovh-off.gif";
	ovh_ovr = new Image(121,35);
	ovh_ovr.src = "/images/battle/war2/images/strat-menu/ovh-ovr.gif";
	ovh_act = new Image(121,35);
	ovh_act.src = "/images/battle/war2/images/strat-menu/ovh-act.gif";

	units_off = new Image(121,35);
	units_off.src = "/images/battle/war2/images/strat-menu/units-off.gif";
	units_ovr = new Image(121,35);
	units_ovr.src = "/images/battle/war2/images/strat-menu/units-ovr.gif";
	units_act = new Image(121,35);
	units_act.src = "/images/battle/war2/images/strat-menu/units-act.gif";

	gen_off = new Image(121,35);
	gen_off.src = "/images/battle/war2/images/strat-menu/gen-off.gif";
	gen_ovr = new Image(121,35);
	gen_ovr.src = "/images/battle/war2/images/strat-menu/gen-ovr.gif";
	gen_act = new Image(121,35);
	gen_act.src = "/images/battle/war2/images/strat-menu/gen-act.gif";

	spell_off = new Image(121,35);
	spell_off.src = "/images/battle/war2/images/strat-menu/spell-off.gif";
	spell_ovr = new Image(121,35);
	spell_ovr.src = "/images/battle/war2/images/strat-menu/spell-ovr.gif";
	spell_act = new Image(121,35);
	spell_act.src = "/images/battle/war2/images/strat-menu/spell-act.gif";

	water_off = new Image(121,35);
	water_off.src = "/images/battle/war2/images/strat-menu/water-off.gif";
	water_ovr = new Image(121,35);
	water_ovr.src = "/images/battle/war2/images/strat-menu/water-ovr.gif";
	water_act = new Image(121,35);
	water_act.src = "/images/battle/war2/images/strat-menu/water-act.gif";

	astrat_off = new Image(121,35);
	astrat_off.src = "/images/battle/war2/images/strat-menu/astrat-off.gif";
	astrat_ovr = new Image(121,35);
	astrat_ovr.src = "/images/battle/war2/images/strat-menu/astrat-ovr.gif";
	astrat_act = new Image(121,35);
	astrat_act.src = "/images/battle/war2/images/strat-menu/astrat-act.gif";

	peasant_off = new Image(233,51);
	peasant_off.src = "/images/battle/war2/images/peasant-off.gif";
	peasant_ovr = new Image(233,51);
	peasant_ovr.src = "/images/battle/war2/images/peasant-ovr.gif";

	prevarw_off = new Image(28,21);
	prevarw_off.src = "/images/battle/war2/images/prevarw-off.gif";
	prevarw_ovr = new Image(28,21);
	prevarw_ovr.src = "/images/battle/war2/images/prevarw-ovr.gif";

	Peon_off = new Image(233,51);
	Peon_off.src = "/images/battle/war2/images/Peon-off.gif";
	Peon_ovr = new Image(233,51);
	Peon_ovr.src = "/images/battle/war2/images/Peon-ovr.gif";

	nextarw_off = new Image(28,21);
	nextarw_off.src = "/images/battle/war2/images/nextarw-off.gif";
	nextarw_ovr = new Image(28,21);
	nextarw_ovr.src = "/images/battle/war2/images/nextarw-ovr.gif";
}

function pop( pageName ) {
  popup=open( "/war2/popup/" + pageName + ".htm", "popup", "width=450,height=150,resizeable=yes" );
  clean=0;
}

function endpop() {
  if ( clean!=1 && !popup.closed ) {
    popup.close();
  }
}

function offImg(objName)
{	imgSrc = eval(objName + "_off.src");
	document[objName].src = imgSrc;
}

function ovrImg(objName)
{	imgSrc = eval(objName + "_ovr.src");
	document[objName].src = imgSrc;
}

function actImg(objName)
{	imgSrc = eval(objName + "_act.src");
	document[objName].src = imgSrc;
}
