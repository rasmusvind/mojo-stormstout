var clean=1;
var pageName;
var imageName;

if (document.images) {
  newson 		= new Image(100,47); newson.src = "/images/battle/scc/protoss/pix/menu/p-news.gif";
  newsoff 		= new Image(100,47); newsoff.src = "/images/battle/scc/protoss/pix/menu/i-news.gif";
  genon 		= new Image(100,32); genon.src = "/images/battle/scc/protoss/pix/menu/p-gen.gif";
  genoff 		= new Image(100,32); genoff.src = "/images/battle/scc/protoss/pix/menu/i-gen.gif";
  terron 		= new Image(100,33); terron.src = "/images/battle/scc/protoss/pix/menu/p-terr.gif";
  terroff 		= new Image(100,33); terroff.src = "/images/battle/scc/protoss/pix/menu/i-terr.gif";
  proton 		= new Image(100,32); proton.src = "/images/battle/scc/protoss/pix/menu/p-prot.gif";
  zergon 		= new Image(100,32); zergon.src = "/images/battle/scc/protoss/pix/menu/p-zerg2.gif";
  zergoff 		= new Image(100,32); zergoff.src = "/images/battle/scc/protoss/pix/menu/i-zerg2.gif";
  wallon 		= new Image(100,32); wallon.src = "/images/battle/scc/protoss/pix/menu/p-wall.gif";
  walloff 		= new Image(100,32); walloff.src = "/images/battle/scc/protoss/pix/menu/i-wall.gif";
  battleon 		= new Image(100,32); battleon.src = "/images/battle/scc/protoss/pix/menu/p-battle.gif";
  battleoff 	= new Image(100,32); battleoff.src = "/images/battle/scc/protoss/pix/menu/i-battle.gif";
  cheatson 		= new Image(100,32); cheatson.src = "/images/battle/scc/protoss/pix/menu/p-cheats.gif";
  cheatsoff 	= new Image(100,32); cheatsoff.src = "/images/battle/scc/protoss/pix/menu/i-cheats.gif";
  faqon 		= new Image(100,32); faqon.src = "/images/battle/scc/protoss/pix/menu/p-faq.gif";
  faqoff 		= new Image(100,32); faqoff.src = "/images/battle/scc/protoss/pix/menu/i-faq.gif";
  mapson 		= new Image(100,32); mapson.src = "/images/battle/scc/protoss/pix/menu/p-maps.gif";
  mapsoff 		= new Image(100,32); mapsoff.src = "/images/battle/scc/protoss/pix/menu/i-maps.gif";
  enlargedon 	= new Image(100,34); enlargedon.src = "/images/battle/scc/protoss/pix/menu/p-enlarged.gif";
  enlargedoff 	= new Image(100,34); enlargedoff.src = "/images/battle/scc/protoss/pix/menu/i-enlarged.gif";
  exiton 		= new Image(100,60); exiton.src = "/images/battle/scc/protoss/pix/menu/p-exit.gif";
  exitoff 		= new Image(100,60); exitoff.src = "/images/battle/scc/protoss/pix/menu/i-exit.gif";
  backon		= new Image(90,60); backon.src = "/images/battle/scc/protoss/pix/menu/p-back.gif";
  backoff		= new Image(90,60); backoff.src = "/images/battle/scc/protoss/pix/menu/i-back.gif";
  nexton		= new Image(100,60); nexton.src = "/images/battle/scc/protoss/pix/menu/p-next.gif";
  nextoff		= new Image(100,60); nextoff.src = "/images/battle/scc/protoss/pix/menu/i-next.gif";

  scouton		= new Image(100,60); scouton.src = "/images/battle/scc/protoss/pix/units/scout1-on.gif";
  scoutoff		= new Image(100,60); scoutoff.src = "/images/battle/scc/protoss/pix/units/scout1-off.gif";
}

function pop( pageName ) {
  popup=open( "/scc/protoss/popup/" + pageName + ".htm", "popup", "width=350,height=400,resizeable=no" );
  clean=0;
}

function smallpop( pageName ) {
  popup=open( "/scc/protoss/popup/" + pageName + ".htm", "popup", "width=300,height=100,resizeable=no" );
  clean=0;
}

function endpop() {
  if ( clean!=1 && !popup.closed ) {
    popup.close();
  }
}

function point( imageName ) {
  if ( document.images ) {
    document[ imageName ].src = eval(imageName + "on.src");
  }
}

function nopoint( imageName ) {
  if ( document.images ) {
    document[ imageName ].src = eval(imageName + "off.src");
  }
}
