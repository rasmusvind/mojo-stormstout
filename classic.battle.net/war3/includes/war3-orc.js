var clean=1;
var pageName;
var imageName;

if (document.images) {
  newsovr 			= new Image(135,48); newsovr.src = "/war3/images/orc/menu/news-ovr.gif";
  newsoff 			= new Image(135,48); newsoff.src = "/war3/images/orc/menu/news-off.gif";

  basicsovr 		= new Image(135,45); basicsovr.src = "/war3/images/orc/menu/basics-ovr.gif";
  basicsoff 		= new Image(135,45); basicsoff.src = "/war3/images/orc/menu/basics-off.gif";

  neutralovr 		= new Image(135,45); neutralovr.src = "/war3/images/orc/menu/neutral-ovr.gif";
  neutraloff 		= new Image(135,45); neutraloff.src = "/war3/images/orc/menu/neutral-off.gif";
 
  humanovr 			= new Image(135,45); humanovr.src = "/war3/images/orc/menu/human-ovr.gif";
  humanoff 			= new Image(135,45); humanoff.src = "/war3/images/orc/menu/human-off.gif";

  orcovr 			= new Image(135,45); orcovr.src = "/war3/images/orc/menu/orc-ovr.gif";
  orcoff 			= new Image(135,45); orcoff.src = "/war3/images/orc/menu/orc-ovr.gif";

  nightelfovr 		= new Image(135,45); nightelfovr.src = "/war3/images/orc/menu/nightelf-ovr.gif";
  nightelfoff 		= new Image(135,45); nightelfoff.src = "/war3/images/orc/menu/nightelf-off.gif";

  undeadovr 		= new Image(135,45); undeadovr.src = "/war3/images/orc/menu/undead-ovr.gif";
  undeadoff 		= new Image(135,45); undeadoff.src = "/war3/images/orc/menu/undead-off.gif";
  
  racesovr 			= new Image(135,45); racesovr.src = "/war3/images/orc/menu/races-ovr.gif";
  racesoff 			= new Image(135,45); racesoff.src = "/war3/images/orc/menu/races-off.gif";
  
  tournamentsovr 	= new Image(135,45); tournamentsovr.src = "/war3/images/orc/menu/tournaments-ovr.gif";
  tournamentsoff 	= new Image(135,45); tournamentsoff.src = "/war3/images/orc/menu/tournaments-off.gif"; 
  
  ladderovr			= new Image(135,45); ladderovr.src = "/war3/images/orc/menu/ladder-ovr.gif";
  ladderoff			= new Image(135,45); ladderoff.src = "/war3/images/orc/menu/ladder-off.gif";
 
  mapsovr 			= new Image(135,45); mapsovr.src = "/war3/images/orc/menu/maps-ovr.gif";
  mapsoff 			= new Image(135,45); mapsoff.src = "/war3/images/orc/menu/maps-off.gif";

  faqovr 			= new Image(135,45); faqovr.src = "/war3/images/orc/menu/faq-ovr.gif";
  faqoff 			= new Image(135,45); faqoff.src = "/war3/images/orc/menu/faq-off.gif";

  cheatsovr 		= new Image(135,45); cheatsovr.src = "/war3/images/orc/menu/cheats-ovr.gif";
  cheatsoff 		= new Image(135,45); cheatsoff.src = "/war3/images/orc/menu/cheats-off.gif";
    
  filesovr 			= new Image(135,45); filesovr.src = "/war3/images/orc/menu/files-ovr.gif";
  filesoff 			= new Image(135,45); filesoff.src = "/war3/images/orc/menu/files-off.gif";
  
  prevovr 			= new Image(38,60); prevovr.src = "/war3/images/orc/prev-ovr.gif";
  prevoff 			= new Image(38,60); prevoff.src = "/war3/images/orc/prev-off.gif";

  nextovr 			= new Image(36,60); nextovr.src = "/war3/images/orc/next-ovr.gif";
  nextoff 			= new Image(36,60); nextoff.src = "/war3/images/orc/next-off.gif";
}

function ovrImg( imageName ) {
  if ( document.images ) {
    document[ imageName ].src = eval(imageName + "ovr.src");
  }
}

function offImg( imageName ) {
  if ( document.images ) {
    document[ imageName ].src = eval(imageName + "off.src");
  }
}

function pop( pageName ) {
  popup=open( "popup/" + pageName + ".htm", "popup", "width=700,height=500,resizeable=no" );
  clean=0;
}
