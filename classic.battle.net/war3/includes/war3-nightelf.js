var clean=1;
var pageName;
var imageName;

if (document.images) {
  newsovr 			= new Image(130,33); newsovr.src = "/war3/images/nightelf/menu/news-ovr.gif";
  newsoff 			= new Image(130,33); newsoff.src = "/war3/images/nightelf/menu/news-off.gif";

  basicsovr 		= new Image(130,32); basicsovr.src = "/war3/images/nightelf/menu/basics-ovr.gif";
  basicsoff 		= new Image(130,32); basicsoff.src = "/war3/images/nightelf/menu/basics-off.gif";

  neutralovr 		= new Image(130,32); neutralovr.src = "/war3/images/nightelf/menu/neutral-ovr.gif";
  neutraloff 		= new Image(130,32); neutraloff.src = "/war3/images/nightelf/menu/neutral-off.gif";
  
  humanovr 			= new Image(130,32); humanovr.src = "/war3/images/nightelf/menu/human-ovr.gif";
  humanoff 			= new Image(130,32); humanoff.src = "/war3/images/nightelf/menu/human-off.gif";

  orcovr 			= new Image(130,32); orcovr.src = "/war3/images/nightelf/menu/orc-ovr.gif";
  orcoff 			= new Image(130,32); orcoff.src = "/war3/images/nightelf/menu/orc-off.gif";
  
  nightelfovr 		= new Image(130,32); nightelfovr.src = "/war3/images/nightelf/menu/nightelf-ovr.gif";
  nightelfoff 		= new Image(130,32); nightelfoff.src = "/war3/images/nightelf/menu/nightelf-off.gif";

  racesovr 			= new Image(130,32); racesovr.src = "/war3/images/nightelf/menu/races-ovr.gif";
  racesoff 			= new Image(130,32); racesoff.src = "/war3/images/nightelf/menu/races-off.gif";
  
  tournamentsovr 	= new Image(130,32); tournamentsovr.src = "/war3/images/nightelf/menu/tournaments-ovr.gif";
  tournamentsoff 	= new Image(130,32); tournamentsoff.src = "/war3/images/nightelf/menu/tournaments-off.gif";
  
  undeadovr 		= new Image(130,32); undeadovr.src = "/war3/images/nightelf/menu/undead-ovr.gif";
  undeadoff 		= new Image(130,32); undeadoff.src = "/war3/images/nightelf/menu/undead-off.gif";

  ladderovr			= new Image(130,32); ladderovr.src = "/war3/images/nightelf/menu/ladder-ovr.gif";
  ladderoff			= new Image(130,32); ladderoff.src = "/war3/images/nightelf/menu/ladder-off.gif";
  
  mapsovr 			= new Image(130,32); mapsovr.src = "/war3/images/nightelf/menu/maps-ovr.gif";
  mapsoff 			= new Image(130,32); mapsoff.src = "/war3/images/nightelf/menu/maps-off.gif";

  faqovr 			= new Image(130,32); faqovr.src = "/war3/images/nightelf/menu/faq-ovr.gif";
  faqoff 			= new Image(130,32); faqoff.src = "/war3/images/nightelf/menu/faq-off.gif";

  cheatsovr 			= new Image(130,32); cheatsovr.src = "/war3/images/nightelf/menu/cheats-ovr.gif";
  cheatsoff 			= new Image(130,32); cheatsoff.src = "/war3/images/nightelf/menu/cheats-off.gif";
   
  filesovr 			= new Image(130,32); filesovr.src = "/war3/images/nightelf/menu/files-ovr.gif";
  filesoff 			= new Image(130,32); filesoff.src = "/war3/images/nightelf/menu/files-off.gif";

}

function ovrImg( imageName ) {
  if ( document.images ) {
    document[ imageName ].src = eval(imageName + "ovr.src");
  }
}

function offImg( imageName ) {
  if ( document.images ) {
    document[ imageName ].src = eval(imageName + "off.src");
  }
}

function pop( pageName ) {
  popup=open( "popup/" + pageName + ".htm", "popup", "width=700,height=500,resizeable=no" );
  clean=0;
}
