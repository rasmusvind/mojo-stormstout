var clean=1;
var pageName;
var imageName;

if (document.images) {
  newsovr 			= new Image(137,37); newsovr.src = "/war3/images/undead/menu/news_on.gif";
  newsoff 			= new Image(137,37); newsoff.src = "/war3/images/undead/menu/news.gif";

  basicsovr 		= new Image(137,33); basicsovr.src = "/war3/images/undead/menu/basics_on.gif";
  basicsoff 		= new Image(137,33); basicsoff.src = "/war3/images/undead/menu/basics.gif";

  neutralovr 		= new Image(137,32); neutralovr.src = "/war3/images/undead/menu/neutral_on.gif";
  neutraloff 		= new Image(137,32); neutraloff.src = "/war3/images/undead/menu/neutral.gif";
  
  humanovr 			= new Image(137,31); humanovr.src = "/war3/images/undead/menu/human_on.gif";
  humanoff 			= new Image(137,31); humanoff.src = "/war3/images/undead/menu/human.gif";

  orcovr 			= new Image(137,32); orcovr.src = "/war3/images/undead/menu/orc_on.gif";
  orcoff 			= new Image(137,32); orcoff.src = "/war3/images/undead/menu/orc.gif";

  racesovr 			= new Image(137,32); racesovr.src = "/war3/images/undead/menu/races_on.gif";
  racesoff 			= new Image(137,32); racesoff.src = "/war3/images/undead/menu/races.gif";

  tournamentsovr	= new Image(137,32); tournamentsovr.src = "/war3/images/undead/menu/tournaments_on.gif";
  tournamentsoff 	= new Image(137,32); tournamentsoff.src = "/war3/images/undead/menu/tournaments.gif";
  
  nightelfovr 		= new Image(137,32); nightelfovr.src = "/war3/images/undead/menu/nightelf_on.gif";
  nightelfoff 		= new Image(137,32); nightelfoff.src = "/war3/images/undead/menu/nightelf.gif";

  undeadovr 		= new Image(137,33); undeadovr.src = "/war3/images/undead/menu/undead_on.gif";
  undeadoff 		= new Image(137,33); undeadoff.src = "/war3/images/undead/menu/undead.gif";

  ladderovr			= new Image(137,32); ladderovr.src = "/war3/images/undead/menu/ladder_on.gif";
  ladderoff			= new Image(137,32); ladderoff.src = "/war3/images/undead/menu/ladder.gif";
  
  mapsovr 			= new Image(137,32); mapsovr.src = "/war3/images/undead/menu/maps_on.gif";
  mapsoff 			= new Image(137,32); mapsoff.src = "/war3/images/undead/menu/maps.gif";

  faqovr 			= new Image(137,31); faqovr.src = "/war3/images/undead/menu/faq_on.gif";
  faqoff 			= new Image(137,31); faqoff.src = "/war3/images/undead/menu/faq.gif";

  cheatsovr 		= new Image(137,31); cheatsovr.src = "/war3/images/undead/menu/cheats_on.gif";
  cheatsoff 		= new Image(137,31); cheatsoff.src = "/war3/images/undead/menu/cheats.gif";
  
  filesovr 			= new Image(137,34); filesovr.src = "/war3/images/undead/menu/files_on.gif";
  filesoff 			= new Image(137,34); filesoff.src = "/war3/images/undead/menu/files.gif";

}

function ovrImg( imageName ) {
  if ( document.images ) {
    document[ imageName ].src = eval(imageName + "ovr.src");
  }
}

function offImg( imageName ) {
  if ( document.images ) {
    document[ imageName ].src = eval(imageName + "off.src");
  }
}

function pop( pageName ) {
  popup=open( "popup/" + pageName + ".htm", "popup", "width=700,height=500,resizeable=no" );
  clean=0;
}
