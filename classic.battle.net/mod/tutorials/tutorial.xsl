<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">

<html>
<head>
  <link rel="stylesheet" type="text/css" href="CSS.css"/>
  <script language="javascript1.2" type="text/javascript" src="main15.js"/>
</head>

<body>
<div id="tiplayer" style="visibility:hidden;position:absolute;z-index:1000;top:-100;"></div>
<div id="headerbg"></div>
<div id="header">
  <div id="bnetLogo"><a href="http://www.battle.net/"><img src="images/bnetLogo.jpg"/></a></div>
  <div id="adBorder">
    <div id="ad">
      <a href="#">
        <img src="images/adSpace.jpg"/>
      </a>
    </div>
  </div>
  <div id="return">
    <a href="/mod/">
      <img src="images/return.jpg"/>
    </a>
  </div>
</div>
<center>
<div id="main">
  <xsl:if test="tutorial/attachments/image/@src != ''">
    <a href="#figures">Figures</a> |
  </xsl:if>
  <xsl:if test="tutorial/attachments/map/src != ''">
    <a href="#attachments">Attachments</a> |
  </xsl:if>
  <xsl:if test="tutorial/code/@on = '1'">
    <a href="#triggers">Triggers</a> |
  </xsl:if>
  <a href="#conclusion">Conclusion</a>
  <xsl:apply-templates/>
</div>

<div id="footer"><a href="http://www.blizzard.com"><img src="images/blizzFooter.jpg"/></a></div>


</center>

</body>
</html>
</xsl:template>

<xsl:template match="header">
<div id="content">
  <p class="title"><xsl:value-of select="title"/></p>
  <p class="author">by <xsl:value-of select="author"/> with edits by
    <script language="JavaScript1.2" >
      Text[0]=["Edits by Blizzard","Sometimes we need to edit the original tutorial for formatting purposes.  We try to keep the original content as intact as possible."]
      Style[0]=["#075DE4","#000000","","","Arial",,"#075DE4","#000000","","","Arial",,,,1,"#075DE4",2,24,0.5,0,2,"gray",,2,,13]
      var TipId="tiplayer"
      var FiltersEnabled = 0
      mig_clay()
    </script><a href="#" onMouseOver="stm(Text[0],Style[0])" onMouseOut="htm()">
    <span class="blizz">Blizzard</span></a>
  </p>
</div>
</xsl:template>



<xsl:template match="text">
  <div id="content">
    <xsl:for-each select="p">
      <xsl:choose>
        <xsl:when test="@type = 'figure'">
          <p>
            <a href="#figures">
              <img>
                <xsl:attribute name="src">
                  <xsl:value-of select="."/>
                </xsl:attribute>
              </img>
            </a>
          </p>
        </xsl:when>
        <xsl:when test="@type = 'attachment'">
          <p>
            <a>
              <xsl:attribute name="href">
                <xsl:value-of select="src"/>
              </xsl:attribute>
              <xsl:value-of select="text"/>
            </a>
          </p>
        </xsl:when>
        <xsl:when test="@type = 'list'">
          <p>
            <xsl:for-each select="li">
              <li><xsl:value-of select="."/></li>
            </xsl:for-each>
          </p>
        </xsl:when>
        <xsl:otherwise>
          <p><xsl:value-of select="."/></p>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </div>
</xsl:template>

<xsl:template match="attachments">
<xsl:if test="image/@src != ''">
  <div id="content">
    <p class="header" name="figures" id="#figures"><a name="figures"/>Figures</p>
      <table>
        <tr>
          <xsl:for-each select="image">
            <td>
              <img>
                <xsl:attribute name="src">
                  <xsl:value-of select="@src"/>
                </xsl:attribute>
              </img>
            </td>
          </xsl:for-each>
        </tr>
        <tr>
          <xsl:for-each select="image">
            <td align="center">Figure<xsl:value-of select="@number"/></td>
          </xsl:for-each>
        </tr>
      </table>
   </div>
</xsl:if>
<xsl:if test="map/src != ''">
  <div id="content">
    <p class="header" name="#maps" id="#maps"><a name="attachments"/>Attachments</p>
    <xsl:for-each select="map">
      <li>
        <a>
          <xsl:attribute name="href">
            <xsl:value-of select="src"/>
          </xsl:attribute>
          <xsl:value-of select="@name"/>
        </a>
      </li>
    </xsl:for-each>
  </div>
</xsl:if>
</xsl:template>

<xsl:template match="code">
  <xsl:if test="@on = 1">
  <div id="content">
    <p class="header"><a name="triggers"/>Triggers</p>
    <p class="tname">Variables</p>
    <table>
      <tr align="center">
        <th>Name</th>
        <th>Type</th>
        <th>Initial Value</th>
      </tr>
      <xsl:for-each select="variable">
        <tr align="center">
          <td class="even"><xsl:value-of select="@name"/></td>
          <td><xsl:value-of select="@type"/></td>
          <td class="even"><xsl:value-of select="@value"/></td>
        </tr>
      </xsl:for-each>
    </table>

    <xsl:for-each select="variable">
      <xsl:choose>
      <xsl:when test="p != ''">
        <p><xsl:value-of select="@name"/> - <xsl:value-of select="p"/></p>
      </xsl:when>
      <xsl:when test="p = ''"> </xsl:when>
    </xsl:choose>
    </xsl:for-each>
    <xsl:for-each select="trigger">
      <p class="tname"><xsl:value-of select="@name"/></p>
      <p class="theader">Events</p>
      <p><pre class="event"><xsl:value-of select="events"/></pre></p>
      <p class="theader">Conditions</p>
      <p><pre class="condition"><xsl:value-of select="conditions"/></pre></p>
      <p class="theader">Actions</p>
      <p><pre class="action"><xsl:value-of select="actions"/></pre></p>
      <xsl:for-each select="comments/p">
        <p class="comment"><xsl:value-of select="."/></p>
      </xsl:for-each>
    </xsl:for-each>
  </div>
  </xsl:if>
</xsl:template>

<xsl:template match="footer">
<div id="content">
  <p><a name="conclusion"/><xsl:value-of select="comment"/></p>
  <p>
    <a target="_new">
      <xsl:attribute name="href">
        <xsl:value-of select="email"/>
      </xsl:attribute>
      <xsl:value-of select="email/@name"/>
    </a>
  </p>
</div>
</xsl:template>


</xsl:stylesheet>
