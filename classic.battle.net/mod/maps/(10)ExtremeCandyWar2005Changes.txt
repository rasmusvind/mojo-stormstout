-----------------------------------------------------------------------------
Extreme Candy War 2005 Changes
-----------------------------------------------------------------------------

Overall Revisions
- Gameplay speed increased dramatically
- Game is more teamwork oriented than hero kill orientated
- Many sound files were removed, reducing size of map by 50%
- Intro cinematic removed
- "Artifact" item classification removed from game
- "Illegal" item shop moved to center of bottom lane and features many new
and powerful items, most notably "TO THE LIMIT" which double spawns mobs in 
your lane
- Max Level increased from Level 10 to 15
- Many abilities scale up in power in several ways at level 3
- Lots of unused items removed, new ones added. Tooltips for all items and 
abilities added and are much easier to read
- Aeon mobs increased in power
- Players start with a Toy Penguin instead of a Scroll of Teleportation
- Candy Monsters move much faster than before to ease the burden of pushing 
them. They also went on a diet, and as a result are smaller- taking up less
pathing space

Specific Gameplay changes
- Hero Selection streamlined
- Attack Speed Bonus per AGI reduced from 2% to 1%
- Multiboard has all player levels, allies and opponents
- Cooldown on all items and trinkets reduced greatly
- Many spell cooldowns reduced greatly
- A ton of bug fixes, most notably the upgrade +1 agi bug, frost nova killing
buildings, and multiboard issues
- Tower structure health reduced greatly
- Periodic Income increases 
- Upgrades are much cheaper overall and their prices more accurately reflect
their potency
- Hero Experience gain increased greatly
- Game messages are displayed on the screen for a much shorter period of time
- Death system totally reworked, you no longer need to run to your corpse,
however, you will be "Feebleminded" for a period of time after you ressurect.
- Descriptive floating text displayed on Hero after every ability cast
- Descriptive floating text displayed on all shops
- Creeps no longer drop loot, their bounty has been increased greatly
- Orb of Haste changed to Evocation of Haste, a five charge item
- Gold from killing enemy heroes reduced, some money is distributed to 
all players of the killing team
- Destroying an enemy barrack will spawn two very powerful mobs for the 
opposing team. It is very important to push back!
- Health regeneration from fountain increased
- Shop placement better properly aligned
- Heroes all have models with "glow" to them
- Better spell damage balance
- Some cheesy or useless items removed from game

Rogue Changes
- Rogue combo points made to be more usable. A rogue stores combo points for
himself, and can utilize them on any target so long as the combo points don't
wear out.
- Rogue stealth is broken if they are attacked while stealthing (2 second
transition) or are spotted by detectors (enemy towers, hunter pet level 3, 
warlock pet level 2 and level 3)
- Rogue evasion made into a levelable ability, stealth moved to innate skill
- Stealth no longer drops movement rate by any percentage

Mage Changes
- Blink replaces Ice Armor
- Mana Shield efficiency greatly improved
- Fireball range improved
- Casting Frost Nova removes Stealth in a large AOE

Warrior Changes
- Intercept replaces Charge and costs rage instead of generating it
- Mortal Strike power increased slightly
- Thunder Clap power slightly reduced
- Overall HP/armor on Warriors greatly increased

Hunter Changes
- Concussive Shot and Poison Shot combined into Concussive Sting, which slows,
DoT's, and deals initial damage.
- Hunters given ability "Hunter's Pet" which is a powerful tanking summon
- Frost Trap greatly improved in cooldown and usability
- Expert Aim slightly reduced in power
- Hunters given ability Volley to replace Wing Clip. Wing clip moved to innate

Shaman Changes
- Frost Shock replaces Earthbind Totem
- Searing Snake Totem replaces Purge
- Chain Lightning increased in power

Priest Changes
- Mind "Flog" replaces Heal 
- Flash Heal moved to innate skill
- Spirit Tap potency slightly reduced

Druid
- Mark of the Thorns power greatly increased
- Tranquility greatly improved
- Innervate replaced Faerie Fire

Paladin
- Hammer of Justice deals no damage and costs no mana
- Lay on Hands changed to heal PLD's max HP
- Paladin aura no longer takes up "2" command buttons
- Paladin got Consecrate

Warlock
- Summon Succubus replaces Drain Life
- Shadow Bolt replaces Fear
- Warlocks got Curse of Exhaustion
- Casting Hellfire removes Stealth in a large AOE