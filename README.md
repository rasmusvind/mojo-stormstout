# Backup of Classic Battlenet

This is mostly a [wget](https://www.gnu.org/software/wget/) backup of [classic.battle.net](http://classic.battle.net) with the following changes:
- Undead Sacrificial pit can now be clicked.
- Some Starcraft wallpapers are now downloadable.
- Classic battle.net menu items are now clickable.
- Many case-insensitive URLs are converted to the correct case.
- Hosting assets from `ftp.blizzard.com` and `world-editor-tutorials.thehelper.net` locally.
- Tons of link rewrites to be absolute but domainless.

You can view a copy of the site [here](https://classic.hiveworkshop.com/strategy.shtml).

## Install

- Most URLs are absolute and start with `/` so unless you wish to rewrite tons of them, give this site its own domain.
- Upload the contents of `classic.battle.net` to the root of the site.
- Upload `ftp.blizzard.com` so it resides as such `/ftp.blizzard.com`.
- Upload `world-editor-tutorials.thehelper.net` so it resides as such `/world-editor-tutorials.thehelper.net`.
- If using Nginx, I had good experience with the following setup:

```
server {
  server_name classic.your-site.com;
  listen 80;

  location ~* \.aspx$ {
    add_header Content-Type text/html;
  }

  charset utf-8;

  root /root-of-the-site;
}
```

